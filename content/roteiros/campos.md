+++
title = 'Campos do Jordão'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

Este roteiro foi projetado para quem busca uma escapada relaxante nas montanhas de Campos do Jordão, conhecida como a "Suíça Brasileira". A cidade oferece charme europeu, belas paisagens naturais e uma rica cena gastronômica.

## Dia 1: Descobrindo o Centro de Campos do Jordão

### Manhã
- **9h00:** Chegada a Campos do Jordão e acomodação no seu hotel.
- **10h30:** Passeio pela Vila Capivari, o coração da cidade, repleta de lojas encantadoras e arquitetura alpina.
- **12h30:** Almoço em um dos restaurantes locais que servem fondues deliciosos.

### Tarde
- **14h00:** Visita ao Mosteiro de São João, um lugar sereno com belas paisagens.
- **16h00:** Degustação de cervejas artesanais em uma das cervejarias da região.
- **18h00:** Retorno ao hotel para relaxar antes do jantar.

### Noite
- **20h00:** Jantar em um restaurante aconchegante em Capivari, seguido de um passeio noturno pela praça iluminada.

## Dia 2: Explorando a Natureza

### Manhã
- **8h30:** Café da manhã no hotel.
- **10h00:** Passeio de teleférico até o Morro do Elefante para vistas panorâmicas.
- **12h00:** Almoço em um restaurante com vista para o Vale do Lajeado.

### Tarde
- **14h00:** Visita ao Horto Florestal, onde você pode fazer trilhas e apreciar a natureza exuberante.
- **16h30:** Degustação de chocolates locais em uma chocolateria.

### Noite
- **19h00:** Jantar em um restaurante sofisticado com pratos gourmet.
- **21h00:** Retorno ao hotel para descansar antes de partir.

Este roteiro é ideal para casais em busca de um fim de semana romântico, amigos que querem relaxar em um ambiente tranquilo ou qualquer pessoa que aprecie a beleza da natureza e uma experiência gastronômica única. Campos do Jordão oferece uma variedade de atividades para todos os gostos, tornando-o um destino atraente para todas as idades. Aproveite sua viagem! 🌲🏔️🍷🍫