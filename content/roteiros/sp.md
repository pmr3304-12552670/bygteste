+++
title = 'São Paulo'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

São Paulo é conhecida por sua vida noturna vibrante, restaurantes incríveis e uma cena cultural pulsante. Este roteiro de final de semana foi criado para quem deseja aproveitar ao máximo a cidade que nunca dorme.

## Dia 1: Descobrindo a Cidade

### Manhã
- **9h00:** Chegada a São Paulo e acomodação em seu hotel.
- **10h30:** Visita ao MASP (Museu de Arte de São Paulo) para apreciar a arte e a arquitetura icônicas.
- **12h30:** Almoço no restaurante **"Figueira Rubaiyat"**. Peça a famosa picanha na pedra. 

### Tarde
- **14h30:** Passeio pelo Parque Ibirapuera, onde você pode relaxar e apreciar a natureza. 
- **16h30:** Visita ao bairro da Liberdade, conhecido por sua cultura japonesa, e aproveite para tomar um café em uma das cafeterias locais. 

### Noite
- **20h00:** Jantar no **"D.O.M."**, um dos melhores restaurantes da América Latina. Experimente o menu degustação. 
- **22h00:** Aproveite a vida noturna da Vila Madalena, com bares animados e música ao vivo. 

## Dia 2: Explorando a Cultura Gastronômica

### Manhã
- **9h30:** Café da manhã na **"Padaria Bella Paulista"**, onde você pode experimentar pães frescos e pasteis de nata. 
- **11h00:** Visita ao Mercado Municipal de São Paulo, onde você pode experimentar o famoso sanduíche de mortadela. 

### Tarde
- **13h00:** Almoço no bairro do Bixiga, no restaurante **"Cantina do Piero"**. Peça uma autêntica lasanha bolonhesa. 
- **15h00:** Visita ao Museu da Língua Portuguesa para aprender mais sobre a cultura brasileira e a língua portuguesa. 

### Noite
- **20h00:** Jantar no **"Tordesilhas"**, um restaurante que oferece pratos brasileiros tradicionais. Experimente a moqueca.
- **22h00:** Explore a região da Rua Augusta, repleta de bares, baladas e casas de show para curtir a noite paulistana. 

Este roteiro é ideal para amantes da gastronomia e da vida noturna. São Paulo oferece uma rica diversidade culinária e uma vida noturna agitada, tornando-a o destino perfeito para quem quer saborear a cultura e a animação da cidade. Aproveite sua estadia na metrópole! 🌃🍽️🎶
