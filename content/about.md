+++
title = 'Nossa História'
date = 2023-09-25T17:59:36-03:00
draft = false
+++

BYG é um projeto resultante da paixão de 5 estudantes por viajar e explorar o mundo. Nosso objetivo desde o início é usar a tecnologia a nosso favor para proporcionar uma experiência única aos nossos usuários para que eles possam criar lembranças e histórias a se contar sem ter que se preocupar com a parte burocrática de viajar. Podendo desfrutar com maior intensidade da experiência que estão vivenciando. 

Nós acreditamos que diferentes pessoas tem diferentes meios de assimilar e interagir com o seu redor, por isso criamos uma ferramenta que vai facilitar e melhorar a vida de todos ao viajarem. Através da conexão de culturas e pessoas nós podemos tornar o mundo um lugar melhor.